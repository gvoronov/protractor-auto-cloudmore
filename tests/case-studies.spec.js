const caseStudiesPage = require('../pages/case-studies.page');
const commonPage = require('../pages/common.page');

describe('Case Studies page', () => {

  beforeEach(() => {
    caseStudiesPage.get();
  });

  it('should display company logo', () => {
    expect(commonPage.getCompanyLogo().isDisplayed()).toBe(true);
  });

});