const contactUsPage = require('../pages/contact-us.page');
const commonPage = require('../pages/common.page');

describe('Contact Us page', () => {

  beforeEach(() => {
    contactUsPage.get();
  });

  it('should display company logo', () => {
    expect(commonPage.getCompanyLogo().isDisplayed()).toBe(true);
  });

  it('should display form in body', () => {
    expect(contactUsPage.form.isDisplayed()).toBe(true);
  });

  it('should display form in footer', () => {
    expect(commonPage.footerForm.isDisplayed()).toBe(true);
  });

});