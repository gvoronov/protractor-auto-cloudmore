const commonPage = require('../pages/common.page');
const homePage = require('../pages/home.page');
const searchResultsPage = require('../pages/search-results.page');
const helper = require('../utils/helper');

describe('Search Results page', () => {

  beforeEach(() => {
    homePage.get();
  });

  it('should display search results', async () => {
    await commonPage.search('Högset');
    
    expect(searchResultsPage.noResults.isPresent()).toBe(false);
    expect(searchResultsPage.getSearchResults().count()).not.toBeLessThan(1);
  });

  it('should take screenshot of 3rd or last page', async () => {
    await commonPage.clickAcceptCookie();
    await commonPage.search('Blog');
    // await commonPage.search('Högset');

    for (i = 0; i < 2; i++) {
      if (await searchResultsPage.nextPageBtn.isPresent()) await searchResultsPage.clickNext();
    }

    await helper.takeScreenshot('desktop');
    await helper.switchToMobileView();
    await helper.takeScreenshot('mobile');
    expect(await searchResultsPage.getSearchResults().count()).not.toBeLessThan(1);
  });

});