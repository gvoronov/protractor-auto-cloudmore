const blogPage = require('../pages/blog.page');
const commonPage = require('../pages/common.page');

describe('Blog page', () => {

  beforeEach(() => {
    blogPage.get();
  });

  it('should display company logo', () => {
    expect(commonPage.getCompanyLogo().isDisplayed()).toBe(true);
  });

});