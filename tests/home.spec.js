const homePage = require('../pages/home.page');
const commonPage = require('../pages/common.page');

describe('Home page', () => {

  beforeEach(() => {
    homePage.get();
  });

  it('should display company logo', () => {
    expect(commonPage.getCompanyLogo().isDisplayed()).toBe(true);
  });

  it('should display all menu items', () => {
    expect(commonPage.menuItems.count()).toBe(6);
    expect(commonPage.platformMenuItem.getText()).toEqual('PLATFORM');
    expect(commonPage.solutionsMenuItem.getText()).toEqual('SOLUTIONS');
    expect(commonPage.aboutUsMenuItem.getText()).toEqual('ABOUT US');
    expect(commonPage.contactUsMenuItem.getText()).toEqual('CONTACT US');
    expect(commonPage.blogMenuItem.getText()).toEqual('BLOG');
    expect(commonPage.caseStudiesMenuItem.getText()).toEqual('CASE STUDIES');
  });

});