const aboutUsPage = require('../pages/about-us.page');
const commonPage = require('../pages/common.page');

describe('About Us page', () => {

  beforeEach(() => {
    aboutUsPage.get();
  });

  it('should display company logo', () => {
    expect(commonPage.getCompanyLogo().isDisplayed()).toBe(true);
  });

});