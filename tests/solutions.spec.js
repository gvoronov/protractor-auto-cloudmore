const solutionsPage = require('../pages/solutions.page');
const commonPage = require('../pages/common.page');

describe('Solutions page', () => {

  beforeEach(() => {
    solutionsPage.get();
  });

  it('should display company logo', () => {
    expect(commonPage.getCompanyLogo().isDisplayed()).toBe(true);
  });

});