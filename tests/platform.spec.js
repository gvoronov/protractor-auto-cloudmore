const platformPage = require('../pages/platform.page');
const commonPage = require('../pages/common.page');

describe('Platform page', () => {

  beforeEach(() => {
    platformPage.get();
  });

  it('should display company logo', () => {
    expect(commonPage.getCompanyLogo().isDisplayed()).toBe(true);
  });

});