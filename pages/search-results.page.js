const helper = require('../utils/helper');

class SearchResultsPage {

  noResults = $('.hs-search__no-results > p:first-child');
  searchResults = $$('#hsresults > li');
  nextPageBtn = $('.hs-search-results__next-page');
  previousPageBtn = $('.hs-search-results__prev-page');
  
  async get(text) {
    await browser.get(`https://web.cloudmore.com/hs-search-results?term=${text}`);
  }

  getSearchResults() {
    helper.waitPresenceOf(this.searchResults, 5000);
    return this.searchResults;
  }

  async clickNext() {
    await helper.waitPresenceOf(this.searchResults, 5000);
    await helper.waitPresenceOf(this.nextPageBtn, 5000);
    await helper.scrollTo(this.nextPageBtn);
    await this.nextPageBtn.click();
  }

}

module.exports = new SearchResultsPage();