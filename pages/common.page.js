const helper = require('../utils/helper');

// Elements shared across all pages.
class CommonPage {

  cookieAcceptBtn = $('#hs-en-cookie-confirmation-buttons-area #hs-eu-confirmation-button');

  companyLogo = $('.ptb20 [data-hs-cos-type="logo"] img');

  menuItems = $$('.ptb20 [data-hs-cos-type="menu"] ul:not(.hs-menu-children-wrapper) > li');
  platformMenuItem = $('.ptb20 [data-hs-cos-type="menu"] ul:not(.hs-menu-children-wrapper) > li:nth-child(1)');
  solutionsMenuItem = $('.ptb20 [data-hs-cos-type="menu"] ul:not(.hs-menu-children-wrapper) > li:nth-child(2)');
  aboutUsMenuItem = $('.ptb20 [data-hs-cos-type="menu"] ul:not(.hs-menu-children-wrapper) > li:nth-child(3)');
  contactUsMenuItem = $('.ptb20 [data-hs-cos-type="menu"] ul:not(.hs-menu-children-wrapper) > li:nth-child(4)');
  blogMenuItem = $('.ptb20 [data-hs-cos-type="menu"] ul:not(.hs-menu-children-wrapper) > li:nth-child(5)');
  caseStudiesMenuItem = $('.ptb20 [data-hs-cos-type="menu"] ul:not(.hs-menu-children-wrapper) > li:nth-child(6)');

  footerForm = $('#contact [data-hs-cos-type="form"]');

  searchIcon = $('.ptb20 .fa-search');
  searchInput = $('[action="/hs-search-results"] input');
  searchButton = $('[action="/hs-search-results"] button');

  async clickAcceptCookie() {
    await helper.waitVisibilityOf(this.cookieAcceptBtn, 5000);
    await this.cookieAcceptBtn.click();
  }

  getCompanyLogo() {
    helper.waitVisibilityOf(this.companyLogo, 5000);
    return this.companyLogo;
  }

  async clickSearchIcon() {
    await helper.waitVisibilityOf(this.searchIcon, 5000);
    await this.searchIcon.click();
  }

  async typeInSearchField(text) {
    await helper.waitVisibilityOf(this.searchInput, 5000);
    await this.searchInput.sendKeys(text);
  }
  
  async clickSearchBtn() {
    await helper.waitVisibilityOf(this.searchButton, 5000);
    await this.searchButton.click();
  }

  async search(text) {
    await this.clickSearchIcon();
    await this.typeInSearchField(text);
    await this.clickSearchBtn();
  }

};

module.exports = new CommonPage();