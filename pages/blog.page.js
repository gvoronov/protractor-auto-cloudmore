class BlogPage {

  async get() {
    await browser.get('https://web.cloudmore.com/blog');
  }
  
}

module.exports = new BlogPage();