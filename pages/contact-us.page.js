class ContactUsPage {

  form = $('.vmiddle [data-hs-cos-type="form"]');

  async get() {
    await browser.get('https://web.cloudmore.com/contact-us');
  }
  
}

module.exports = new ContactUsPage();