class CaseStudiesPage {

  async get() {
    await browser.get('https://web.cloudmore.com/case-studies');
  }
  
}

module.exports = new CaseStudiesPage();