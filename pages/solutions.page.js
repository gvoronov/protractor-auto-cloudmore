class SolutionsPage {

  async get() {
    await browser.get('https://web.cloudmore.com/solutions');
  }
  
}

module.exports = new SolutionsPage();