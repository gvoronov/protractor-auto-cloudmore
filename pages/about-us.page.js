class AboutUsPage {

  async get() {
    await browser.get('https://web.cloudmore.com/about-us');
  }
  
}

module.exports = new AboutUsPage();