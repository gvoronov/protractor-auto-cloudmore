# Automation project on Protractor #

Testing https://web.cloudmore.com/

## Prerequisites
Following software should be installed:
* `NodeJS | version 12+`
* `Java JDK | version 8+`

Type following commands to check versions:
* `java -version`
* `node -v`


## Usage

### 1. Install Packages
* ```npm i```

### 2. Install Protractor and WebDriver-Manager globally
* `npm i -g protractor`

Type following command to check version:
* `protractor --version`

### 3. Update drivers
* `webdriver-manager update`

### 4. Run Webdriver-Manager
* `webdriver-manager start`

You can see information about the status of the server at http://localhost:4444/wd/hub


### Run Tests
Run the tests by using one of the following commands:
* `npm run test`
* `protractor config.js`


## Reports
Report is placed to `report` folder with `.html` format.
Screenshots are captured only for failed tests.
Report files are recreated every time you run tests.


## Tech Stack
* JavaScript
* Protractor - e2e Test Framework
  * Jasmine - BDD Test Framework

* Packages:
  * Protractor Screenshot Utils
  * Jasmine Spec Reporter - Console Report
  * Protractor Jasmine2 Screenshot Reporter - HTML Report