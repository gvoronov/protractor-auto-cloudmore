class Helper {
  EC = ExpectedConditions;

  async waitVisibilityOf(element, timeout) {
    await browser.wait(this.EC.visibilityOf(element), timeout);
  }

  async waitInvisibilityOf(element, timeout) {
    await browser.wait(this.EC.invisibilityOf(element), timeout);
  }

  async waitPresenceOf(element, timeout) {
    await browser.wait(this.EC.presenceOf(element), timeout);
  }
  
  async scrollTo(element) {
    await browser.executeScript("arguments[0].scrollIntoView({block:'center'});", element);
  };

  async hideCookieBanner() {
    await browser.executeScript('document.getElementById("hs-eu-cookie-confirmation").style.display="none";');
  }

  async takeScreenshot(name) {
    await screenShotUtils.takeScreenshot({
      saveTo: `report/${name}.png`
    });
  }

  async switchToMobileView() {
    await browser.driver.manage().window().setSize(360, 640);
  }
  
};

module.exports = new Helper();